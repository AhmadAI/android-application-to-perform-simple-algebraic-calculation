package com.example.q7;

import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;

import androidx.core.view.WindowCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.q7.databinding.ActivityMainBinding;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button;
        EditText etext1, etext2;
        TextView rtv;

        button = findViewById(R.id.button);
        etext1 = findViewById(R.id.etext1);
        etext2 = findViewById(R.id.etext2);
        rtv = findViewById(R.id.rtv);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int a = Integer.parseInt(etext1.getText().toString());
                int b = Integer.parseInt(etext2.getText().toString());

                double c = Math.pow((a+b), 2);
                double d = Math.pow((a+b), 3);
                double e = (a*a)-(b*b);


                rtv.setText("  Solutions: \ni."+c+"\nii."+d+"\nii."+e);
            }
        });


    }
}